<?php

function jsonSuccess($data, $code = 200)
{
	return response()->json([
	    'status' => 'success',
	    'data' => $data,
	], $code);
}

function jsonFail($data, $code = 400)
{
	return response()->json([
	    'status' => 'fail',
	    'data' => $data,
	], $code);
}

function jsonError($message, $code = 500)
{
	return response()->json([
	    'status' => 'error',
	    'message' => $message,
	], $code);
}