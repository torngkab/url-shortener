<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Url;

class GenerateCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate shorten url from full url.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        # ask
        $full_url = $this->ask('What is your full URL?');

        # save to database
        $url = new Url;
        $url->code = str_random(6);
        $url->full_url = $full_url;
        $url->save();

        $this->info('Your code is ' . $url->code);
        $this->info('Your shorten URL is ' . url('/') . '/' . $url->code);
    }
}
