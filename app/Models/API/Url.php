<?php

namespace App\Models\API;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class Url
{
	/**
	 * $client
	 */
	protected $client;

	/**
	 * __construct
	 */
	public function __construct()
	{
		$this->client = new Client([
		    'base_uri' => config('app.api_url') . '/',
		    'timeout'  => 5.0,
		]);
	}

	/**
	 * get urls
	 */
	public function get()
	{
		$response = $this->client->request('GET', 'urls', [
			'query' => [
				'api_token' => Auth::user()->api_token,
			],
		]);

		$body = $response->getBody();
		
		$arr = json_decode($body, true);

		return $arr['data'];
	}

	/**
	 * delete url
	 */
	public function destroy($id)
	{
		$response = $this->client->request('DELETE', "urls/{$id}", [
			'query' => [
				'api_token' => Auth::user()->api_token,
			],
		]);

		$body = $response->getBody();
		
		$arr = json_decode($body, true);

		return $arr['data'];
	}
}
