<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\API\Url;

class HomeController extends Controller
{
    /**
     * App\Models\API\Url
     */
    protected $url;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Url $url)
    {
        $this->middleware('auth');
        $this->url = $url;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urls = $this->url->get();

        return view('home', compact('urls'));
    }

    /**
     * Delete url
     */
    public function destroyUrl(Request $request, $id)
    {
        $this->url->destroy($id);

        return redirect()->route('admin');
    }
}
