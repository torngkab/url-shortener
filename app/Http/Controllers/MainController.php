<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Url;
use Carbon\Carbon;
use Validator;

class MainController extends Controller
{
	/**
	 * Home page
	 */
	public function index()
	{
		return view('index');
	}

	/**
	 * Submit your full url
	 */
	public function submit()
	{
		# validate bad url
		$validator = Validator::make(request()->all(), [
            'url' => 'required|url',
        ]);

        if ($validator->fails()) {
            return redirect()->route('index')
                        ->withErrors($validator)
                        ->withInput();
        }

		# save to database
		$url = new Url;
		$url->code = str_random(6);
		$url->full_url = request()->input('url');
		
		if (request()->has('expire')) {
			$url->expire = request()->input('expire');
		}

		$url->save();

		session()->flash('short_url', url('/' . $url->code));

		return redirect()->route('index')->withSuccess('Added Shorten URL!');
	}

	/**
	 * Link to full url
	 */
	public function link(Request $request, $code)
	{
		$url = Url::withTrashed()->where('code', $code)->firstOrFail();

		# check delete link
		if ($url->deleted_at) {
			return abort(410);
		}

		# check expire
		if ($url->expire and Carbon::parse($url->expire)->lessThan(Carbon::now())) {
			return abort(410);
		}

		$url->click += 1;
		$url->save();

		return redirect($url->full_url);
	}
}
