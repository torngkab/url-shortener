<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('admin');
Route::get('/url/{id}/delete', 'HomeController@destroyUrl')->name('admin.url.destroy');

Route::get('/', 'MainController@index')->name('index');
Route::post('/', 'MainController@submit')->name('submit');
Route::get('/{code}', 'MainController@link')->name('link');