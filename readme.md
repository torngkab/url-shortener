**Demo**

https://url.neetztudio.com

-------------------------------------

**Install Guide**

-------------------------------------

***Using XAMPP or MAMP***

1. clone this project

2. composer install

3. copy `.env.example` to `.env`

4. docker-compose up -d (for expose postgresql) [How to install docker for windows](https://docs.docker.com/docker-for-windows/)

5. php artisan migrate

6. make your `vhost` to `url-shortener.local` [How to make vhost](https://www.valuebound.com/resources/blog/how-to-setup-virtual-host-windows-7-xampp-server/)

7. Let's play.


***Using Docker***

1. clone this project

2. docker-compose up -d

3. copy `.env.example` to `.env`

4. docker exec -it url-shortener-web php artisan migrate

5. Let's play on `http://{your-host}:8801`.