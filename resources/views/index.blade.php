<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>URL Shortener</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

  <!-- Styles -->
  <style>
  html, body {
    background-color: #fff;
    color: #636b6f;
    font-family: 'Nunito', sans-serif;
    font-weight: 200;
    height: 100vh;
    margin: 0;
  }

  .full-height {
    height: 100vh;
  }

  .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  .position-ref {
    position: relative;
  }

  .top-right {
    position: absolute;
    right: 10px;
    top: 18px;
  }

  .content {
    text-align: center;
  }

  .title {
    font-size: 34px;
  }

  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
  }

  .m-b-md {
    margin: 30px 0;
  }
</style>
</head>
<body>
  <div class="position-ref full-height">
    <div class="content m-b-md">
      <div class="title">
        @if (session()->has('short_url'))
          Your short URL is 
        @else
          Short URL
        @endif
      </div>
      <br/>
      <div class="col-6 offset-3">
        @if (session()->has('short_url'))
          <div class="title">
            <a href="{{ session('short_url') }}" target="_blank">{{ session('short_url') }}</a>
          </div>
        @else
          <form action="{{ route('submit') }}" method="post" class="">
            <div class="form-group">
              <input type="text" class="form-control" name="url" placeholder="Input your full URL" value="{{ old('url') }}">
            </div>
            <div class="form-group">
              <label for="">Expire</label>
              <input type="datetime-local" class="form-control" name="expire">
            </div>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        @endif
      </div>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

  @if ($errors->any())
    <script>
      toastr.error('{{ $errors->first() }}', 'Error')
    </script>
  @endif

  @if (session()->has('success'))
    <script>
      toastr.success('{{ session('success') }}', 'Success')
    </script>
  @endif
</body>
</html>
