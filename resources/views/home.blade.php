@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>Shorten URL</th>
                            <th>Full URL</th>
                            <th>Click</th>
                            <th>Expired Date</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($urls as $url)
                            <tr>
                                <td>{{ $url['id'] }}</td>
                                <td>
                                    <a href="{{ route('link', $url['code']) }}" target="_blank">
                                        {{ url('/') }}/{{ $url['code'] }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ $url['full_url'] }}" target="_blank">
                                        {{ $url['full_url'] }}
                                    </a>
                                </td>
                                <td>{{ $url['click'] }}</td>
                                <td>{{ $url['expire'] }}</td>
                                <td>
                                    <a href="{{ route('admin.url.destroy', ['id' => $url['id']]) }}"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
